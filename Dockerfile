FROM ubuntu:16.04

RUN apt-get update
RUN dpkg --add-architecture x64
RUN cat /var/lib/dpkg/arch
RUN dpkg --print-architecture
RUN apt-get purge -y ".*:i386"
RUN dpkg --remove-architecture i386
RUN apt-get update && apt-get upgrade -y
RUN apt-get install -y apt-transport-https curl && curl --version
RUN apt-get install -y wget && wget --version

# git
RUN apt-get install -y git

# node
RUN curl -sL https://deb.nodesource.com/setup_6.x | bash -
RUN apt-get install -y nodejs && node --version && npm --version

# yarn
RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN apt-get update
RUN apt-get install -y yarn && yarn --version

# utilities required when building for linux targets in electron-builder
RUN apt-get install -y xz-utils && lzma --version
RUN apt-get install -y binutils && ar --version
RUN apt-get install -y rpm && rpmbuild --version

# wine is required when building for windows targets in electron-builder
#RUN apt-get upgrade -y
#RUN apt-get install -y wine # winetricks
RUN apt-get install -y software-properties-common python-software-properties
RUN wget https://dl.winehq.org/wine-builds/Release.key
RUN apt-key add Release.key
RUN apt-add-repository 'https://dl.winehq.org/wine-builds/ubuntu/ xenial main'
RUN apt-get update
RUN apt-get install -y --install-recommends winehq-stable && wine --version

# required so that electron-builder can cache downloads of binaries between jobs
VOLUME /tmp/electron
