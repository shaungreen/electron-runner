#!/bin/bash
CWD="$(cd $(dirname $0); pwd)"

git add . && \
git commit -m 'committing changes before building image' && \
git push

sudo docker build -t electron-runner $* ${CWD} && \
sudo docker login && \
sudo docker tag  electron-runner shaungreen/electron-runner && \
sudo docker push shaungreen/electron-runner
